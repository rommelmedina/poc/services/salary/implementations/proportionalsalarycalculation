/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */

package com.resolve.proportional.salary.calculation;

import com.resolve.football.models.TeamDto;
import com.resolve.proportional.ProportionalCalculation;
import com.resolve.proportional.models.ProportionalCalculationItem;
import com.resolve.salary.calculation.SalaryCalculation;
import java.util.List;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

/**
 * Serivce of calculation process for proportional salary.
 *
 * @author Rommel Medina
 *
 */
@Service
public class ProportionalSalaryCalculationAdapter implements SalaryCalculation<TeamDto, TeamDto> {
  /**
   * Mapper for request.
   */
  @Autowired
  private Converter<TeamDto, List<ProportionalCalculationItem>> requestMapper;
  
  /**
   * Response mapper.
   */
  @Autowired
  private Converter<Pair<TeamDto, List<ProportionalCalculationItem>>, TeamDto> resultMapper;
  
  /**
   * Proportional Calculation Service.
   */
  @Autowired
  private ProportionalCalculation service;
  
  /**
   * Apply proportional calculation.
   *
   * @param request Team
   * @return Team with salary calculation.
   */
  @Override
  public TeamDto compute(TeamDto request) {
    List<ProportionalCalculationItem> input = this.requestMapper.convert(request);
    
    List<ProportionalCalculationItem> results = this.service.compute(request.getId(), input);
    
    Pair<TeamDto, List<ProportionalCalculationItem>> pairRequest =
        new Pair<TeamDto, List<ProportionalCalculationItem>>(request, results);
    this.resultMapper.convert(pairRequest);
    
    return request;
  }
}
