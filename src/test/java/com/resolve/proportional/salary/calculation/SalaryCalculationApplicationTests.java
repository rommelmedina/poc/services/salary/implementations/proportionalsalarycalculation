package com.resolve.proportional.salary.calculation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.resolve.football.models.TeamDto;
import com.resolve.proportional.definition.provider.mock.MockProportionalDefinitionProviderConfiguration;
import com.resolve.proportional.mappers.ProportionalMapperConfiguration;
import com.resolve.proportional.simple.SimpleProportionalCalculationConfiguration;
import com.resolve.salary.calculation.SalaryCalculation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { ProportionalSalaryCalculationConfiguration.class,
    SimpleProportionalCalculationConfiguration.class, ProportionalMapperConfiguration.class,
    MockProportionalDefinitionProviderConfiguration.class })
@SpringBootTest
class SalaryCalculationApplicationTests {

  @Autowired
  SalaryCalculation<TeamDto, TeamDto> salaryService;

  @Test
  void proportionalSalaryCalculation() {
    // Resuelve FC
    TeamDto teamRfc = new TeamDto();
    teamRfc.setId("ResuelveFC");
    teamRfc.setName(teamRfc.getId());
    // luis Level Cuahu
    teamRfc.addPlayer("Luis", "Cuahu", "ResuelveFC", 50000.0, 10000.0, 19.0);
    // martin Level C
    teamRfc.addPlayer("Martin", "C", "ResuelveFC", 40000.0, 9000.0, 16.0);
    // pedro Level B
    teamRfc.addPlayer("Pedro", "B", "ResuelveFC", 30000.0, 8000.0, 7.0);
    // juan Level A
    teamRfc.addPlayer("Juan", "A", "ResuelveFC", 20000.0, 7000.0, 6.0);
    teamRfc.setProduction(48.0);

    // Rojo
    TeamDto teamRojo = new TeamDto();
    teamRojo.setId("Rojo");
    teamRojo.setName(teamRojo.getId());
    // juanperez
    teamRojo.addPlayer("Juan Perez", "C", "Rojo", 50000.0, 25000.0, 10.0);
    // elrulo
    teamRojo.addPlayer("El Rulo", "B", "Rojo", 30000.0, 15000.0, 9.0);
    teamRojo.setProduction(19.0);

    // Azul
    TeamDto teamAzul = new TeamDto();
    teamAzul.setId("Azul");
    teamAzul.setName(teamAzul.getId());
    // elcuahu
    teamAzul.addPlayer("El Cuahu", "Cuahu", "Azul", 100000.0, 30000.0, 30.0);
    // cosmefulanito
    teamAzul.addPlayer("Cosme Fulanito", "A", "Azul", 30000.0, 10000.0, 7.0);
    teamAzul.setProduction(37.0);

    this.salaryService.compute(teamRfc);
    this.salaryService.compute(teamRojo);
    this.salaryService.compute(teamAzul);

    // Resuelve FC
    // luis Level Cuahu
    assertEquals(59550.00, teamRfc.getPlayer("Luis").getFullSalary());
    // martin Level C
    assertEquals(48820.0, teamRfc.getPlayer("Martin").getFullSalary());
    // pedro Level B
    assertEquals(36640.0, teamRfc.getPlayer("Pedro").getFullSalary());
    // juan Level A
    assertEquals(26860.0, teamRfc.getPlayer("Juan").getFullSalary());

    // Rojo
    // juanperez
    assertEquals(70833.33, teamRojo.getPlayer("Juan Perez").getFullSalary());
    // elrulo
    assertEquals(44250.0, teamRojo.getPlayer("El Rulo").getFullSalary());

    // Azul
    // elcuahu
    assertEquals(130000.0, teamAzul.getPlayer("El Cuahu").getFullSalary());
    // cosmefulanito
    assertEquals(40000.0, teamAzul.getPlayer("Cosme Fulanito").getFullSalary());
  }

}
